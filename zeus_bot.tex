% !TeX root = these.tex

ZeuS is a common name for a family of malwares, consisting of several versions. The first versions of ZeuS appeared at the first time in 2007~\autocite{ZeusNicolas}, and the malware is still actively developed today. The source codes of this malware are for sale in the underground markets, and source codes of some early versions are even leaked online (e.g.~\autocite{ZeusSource}). There exist already several analysis on ZeuS~\autocite{ZeusAnalysis,ZeusNicolas,GameOverZeuS1,GameOverZeuS2}, the following analysis focuses on the stealth techniques used by ZeuS\footnote{The analyzed sample has MD5 hash \texttt{c84b88f6c567b2da651ad683b39ceb2d} and is obtained from \url{https://malwr.com}.}.

% There exist already several analysis on ZeuS~\autocite{ZeusAnalysis,ZeusNicolas}, and source codes of some versions of this are even leaked online (e.g.~\autocite{ZeusSource}). The following analysis focuses on the stealth techniques used by ZeuS. 

Because of technical limits discussed in~\Cref{subsec:limits}, our experimental Pintool cannot detect the execution point where the malware hooked some Windows API(s) to steal credential information, then cannot construct directly the execution tree and the final LTS describing how the malware parses the inputs. We can only test the Pintool on the parser extracted and recompiled from the available source codes. 

However, the obtained results are promising. It suggests that if the technical difficulties about detecting the execution point can be handled, then our approach can be used to deal with real-world malwares. The analysis given in this appendix aim to discuss these technical difficulties, in the hope that giving some perspectives for the future work.

\section{Stealth techniques}

ZeuS does not execute as a explicit and dependent process, it instead injects itself into the memory space of other processes, then execute as a thread of the injected process. The codes in~\Cref{fig:process_snapshot} demonstrates how the malware select a process to inject itself into. It calls the API \texttt{CreateToolhelp32Snapshot} to get a list of currently executed processes, then uses the API \texttt{Process32FirstW/Process32NextW} to traverse this list. For each traversed process, the malware tries to open this process by calling \texttt{OpenProcess} to inject codes (cf.~\Cref{fig:process_open}).

\begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[max width=1\textwidth,keepaspectratio]{process_snapshot.png}
    \caption{Process listing and traversing}
    \label{fig:process_snapshot}
  \end{subfigure}%
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[max width=1\textwidth,keepaspectratio]{inject_code.png}
    \caption{Process opening}
    \label{fig:process_open}
  \end{subfigure}
  \caption{Selecting process for injecting codes}
  \label{fig:process_selection}
\end{figure}

The malware calls the API \texttt{VirtualAllocEx} to allocate new memory inside the memory space of the opened process (cf.~\Cref{fig:mem_allocation}).
% The piece of codes in~\Cref{fig:mem_allocation} demonstrates how the malware allocates new memory inside the memory space of the opened process. 
Next, it calls \texttt{WriteProcessMem\-ory} to copy itself into the allocated memory (cf.~\Cref{fig:code_copy}). Finally, the malware calls the API \texttt{CreateRemoteThread} to activate the injected codes as a new execution thread of the injected process (cf.~\Cref{fig:active_code}).

\begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[max width=1\textwidth,keepaspectratio]{code_alloc.png}
    \caption{Memory allocation}
    \label{fig:mem_allocation}
  \end{subfigure}%
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[max width=1\textwidth,keepaspectratio]{code_copy.png}
    \caption{Writing codes}
    \label{fig:code_copy}
  \end{subfigure}
  \caption{Injecting codes into the opened process}
  \label{fig:mem_allocation_copy}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[max width=1\textwidth,keepaspectratio]{active_thread.png}
  \caption{Activating codes}
  \label{fig:active_code}
\end{figure}

% Our experimental Pintool intercepts some basic network reception functions of Windows (cf.~\Cref{subsec:limits})
\paragraph{Technical difficulties}
The steal techniques discussed above, where ZeuS is a concrete instance, neutralize the function of our Pintool. Concretely, in tracing the execution such a program, the Pintool loses control where the program inserts itself as a (or several) new thread in another process: the Pintool does not know exactly which thread should be traced. 

More importantly, the API interception mechanism of the Pintool functions only at the loading time. First, it scans (by calling Pin's API \texttt{RTN\_FindByName}) the loaded dlls to determine the addresses of intercepted API(s). Then it inserts instrumented functions to intercept two execution points where the API is called and returns (by calling Pin's API \texttt{RTN\_InsertCall}). 

This interception mechanism does not work for programs using steal techniques discussed above because it does not make sense to intercept API(s) at the loading time of the instrumented program. The program may not call these API(s), instead it injects its codes in another process, and use the Windows API \texttt{GetProcAddress} to get directly addresses of these API(s) but in the context of the injected process. 

One may think that the Pintool should instrument the injected process instead of the program. But in this case, the injected process may have already loaded all necessary dlls. Consequently, the loading time interception mechanism of the Pintool does not work again.

\begin{remark}
It may worth noting that the technical difficulties above concern with the input capturing only. It does not concern with the limits of our \emph{stepwise abstraction} procedure and with the limits of message classification using the final LTS. In other words, it does not concern with our approach in message format extraction.
\end{remark}

The general and automatic treatment for these technical difficulties is a must for a real-world malwares analysis tool. Our implemented Pintool is only an experimental prototype, it is currently at the first step toward such a tool. 

\section{Credential information parser}

ZeuS hooks also the Windows API(s) \texttt{send} and \texttt{WSASend} to collect credential information of users in the infected host. To do that, the hooked raw information is parsed also by the malware. We can test out the Pintool on this parser by extracting and recompiling the relevant codes from the source codes of the malware at~\autocite{ZeusSource}. In the following test case, we extract the function \texttt{socketGrabber} of ZeuS, remove irrelevant system calls inside the function, and change the macro \texttt{XOR\_COMPARE} into a character based comparison, recompile it, and put it under the examination of our Pintool.

\begin{figure}[ht]
  \centering
  \begin{adjustbox}{center,max width=1.2\textwidth}
  \includegraphics[max width=1.2\textwidth,keepaspectratio]{dfa_abstracted_zeus.pdf}
  \end{adjustbox}
  \caption{Final LTS of ZeuS with limit length $350$}
  \label{fig:final_lts_zeus}
\end{figure}

The final label transition system in~\Cref{fig:final_lts_zeus} is obtained from the stepwise abstraction procedure where execution traces are limited at length of $350$ instructions (note that $32$ is the ASCII code of the space character). Considering the paths from the initial state to the terminal state, we can observe the following classes of messages are distinguished by the parser
\begin{align*}
   &\tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{n-5} & &\tP \cdot \tA \cdot \tS \cdot \tS \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{n-5} \\
   &\tP \cdot \tA \cdot \tS \cdot \tV \cdot {\tBytes}^{n-4} & &\tP \cdot \tW \cdot \tD \cdot {\tBytes}^{n-4} \\ 
   &\tC \cdot \tW \cdot \tD \cdot {\tBytes}^{n-4} & &\tT \cdot \tY \cdot \tP \cdot \tE \cdot \lbrace !\tA \rbrace \cdot {\tBytes}^{n-5} \\
   &\tF \cdot \tE \cdot \tA \cdot \tT \cdot \lbrace !\tA \rbrace \cdot {\tBytes}^{n-5} & &\tS \cdot \tT \cdot \tA \cdot \tT \cdot \lbrace !\tA \rbrace \cdot \texttt{Byte}^{n-5} \\ 
   &\tL \cdot \tI \cdot \tS \cdot \tT \cdot \lbrace !\tA \rbrace \cdot \texttt{Byte}^{n-5}
\end{align*}
The observed words \texttt{USER}, \texttt{PASS}, \texttt{PASV}, \texttt{PWD}, \texttt{CWD}, \texttt{TYPE}, \texttt{FEAT}, \texttt{STAT}, \texttt{LIST} are commands defined in the \texttt{ftp} protocol. That may be interpreted as an insight that \texttt{ZeuS} steals information concerned with \texttt{ftp} communications. Moreover, besides these words there is no more commands of \texttt{ftp} occurring in the final LTS, so we may think that \texttt{Zeus} is interested only in this subset of \texttt{ftp} commands.

We can observe also the following classes of messages in the final LTS (note that the set $\lbrace 1,2,\dots,31 \rbrace$ contains ASCII codes of all control characters)
\begin{align*}
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \lbrace !\tSP \rbrace \cdot {\tBytes}^{n-5} \\
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{k} \cdot \tCR \cdot {\tBytes}^{n-k-6} \\
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{k} \cdot \tLF \cdot {\tBytes}^{n-k-6} \\
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{k} \cdot \left(\lbrace 1,2\dots,31 \rbrace \setminus \lbrace \tCR,\tLF \rbrace\right) \cdot {\tBytes}^{n-k-6} \\
  & \tP \cdot \tA \cdot \tS \cdot \tS \cdot \lbrace !\tSP \rbrace \cdot {\tBytes}^{n-5} \\
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{k} \cdot \tCR \cdot {\tBytes}^{n-k-6} \\
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{k} \cdot \tLF \cdot {\tBytes}^{n-k-6} \\
  & \tU \cdot \tS \cdot \tE \cdot \tR \cdot \tSP \cdot \lbrace 32,33,\dots,255 \rbrace^{k} \cdot \left(\lbrace 1,2,\dots,31 \rbrace \setminus \lbrace \tCR,\tLF \rbrace\right) \cdot {\tBytes}^{n-k-6}
\end{align*}
These classes show clearly how \texttt{ZeuS} parses a \texttt{USER} or \texttt{PASS} command: it checks whether the next character is space or not, if yes then it collects the bytes until a control character is detected. 
% In other words, we retrieved the delimiter-oriented analysis of Caballero et al.~\cite{Caballero07ccs}. 