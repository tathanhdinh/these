//#include <boost/algorithm/searching/knuth_morris_pratt.hpp>
/*#include <string>
#include <vector>
using namespace std;

vector<int> KMP(string S, string K)
{
	vector<int> T(K.size() + 1, -1);
	vector<int> matches;
 
	if(K.size() == 0)
	{
			matches.push_back(0);
			return matches;
	}
	
	for(int i = 1; i <= K.size(); i++)
	{
		int pos = T[i - 1];
		while(pos != -1 && K[pos] != K[i - 1]) pos = T[pos];
		T[i] = pos + 1;
	}
 
	int sp = 0;
	int kp = 0;
	while(sp < S.size())
	{
		while(kp != -1 && (kp == K.size() || K[kp] != S[sp])) kp = T[kp];
		kp++;
		sp++;
		if(kp == K.size()) matches.push_back(sp - K.size());
	}
 
	return matches;
}


int main(int argc, char* argv[]) 
{
  std::string str_1 = "eebc";
  std::string str_2 = "seebdeebc";
  
//  boost::algorithm::knuth_morris_pratt_search(str_1.begin(), str_1.end(), str_2.begin(), str_2.end());
	KMP(str_1, str_2);
  return 0;
}*/


//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>

/*
int *compute_prefix_function(char *pattern, int psize)
{
	int k = -1;
	int i = 1;
	int *pi = (int*)malloc(sizeof(int)*psize);
	if (!pi)
		return NULL;

	pi[0] = k;
	for (i = 1; i < psize; i++) {
		while (k > -1 && pattern[k+1] != pattern[i])
			k = pi[k];
		if (pattern[i] == pattern[k+1])
			k++;
		pi[i] = k;
	}
	return pi;
}

int kmp(char *target, int tsize, char *pattern, int psize)
{
	int i;
	int *pi = compute_prefix_function(pattern, psize);
	int k = -1;
	if (!pi)
		return -1;
	for (i = 0; i < tsize; i++) {
		while (k > -1 && pattern[k+1] != target[i])
			k = pi[k];
		if (target[i] == pattern[k+1])
			k++;
		if (k == psize - 1) {
			free(pi);
			return i-k;
		}
	}
	free(pi);
	return -1;
}*/

//static int kmp_table[7];

int* pre_table(char* pattern_buf/*, int pattern_size*/, int* kmp_table) 
{
  int i = 0; int j = -1;

  //int* kmp_table = (int*)malloc(sizeof(int) * pattern_size);
	//static int kmp_table[7];
  kmp_table[0] = -1;

  while (i < /*pattern_size*/5)
  {
    while (j > -1 && (pattern_buf[i] != pattern_buf[j]))
    {
      j = kmp_table[j];
    }
    ++i; ++j;
    if (pattern_buf[i] == pattern_buf[j])
    {
      kmp_table[i] = kmp_table[j];
    }
    else
    {
      kmp_table[i] = j;
    }
  }

  return kmp_table;
}

int kmp(char* pattern, /*int pattern_size,*/ char* target/*, int target_length*/)
{
  //int* kmp_table = pre_table(pattern, pattern_size);
	int kmp_table[5];
	pre_table(pattern/*, pattern_size*/,kmp_table);
  int i = 0; int j = 0;

  while (j < /*target_length*/50)
  {
    while (i > -1 && pattern[i] != target[j])
    {
      i = kmp_table[i];
    }
    i++; j++;
    if (i >= /*pattern_size*/5) return (j - i);
  }

  return -1;
}


int main(int argc, const char *argv[])
{
	char target[] = "ABC ABCDAB ABCDABCDABDE";
	char *ch = target;
	char pattern[] = "ABCDABD";
	int i;

//	i = kmp(target, strlen(target), pattern, strlen(pattern));
  i = kmp(pattern, /*strlen(pattern),*/ target/*, strlen(target)*/);
	//if (i >= 0)
	//	printf("matched @: %s\n", ch + i);
	return i;
}
