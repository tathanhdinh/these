set terminal pdf
set output "wget_results.pdf"

set xlabel "limit trace length"
# set xrange []

set ylabel "CFI number"
# set yrange [0:10000]

set key left

set grid xtics ytics

# plot "wget_results.dat" using 1:2 with lines
plot "wget_results.dat" using 1:2 with linespoints title "branchable", \
     "wget_results.dat" using 1:4 with linespoints title "unbranchable", \
     "wget_results.dat" using 1:5 with linespoints title "total"
