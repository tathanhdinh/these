set terminal pdf
# set output "wget_results.pdf"
# set output "nginx_results.pdf"
# set output "jwhois_results.pdf"
# set output "curl_results.pdf"
# set output "netcat_results.pdf"
# set output "links_results.pdf"
# set output "links_time.pdf"
# set output "ncftpget_time.pdf"
set output "wgetftp_time.pdf"

# set output results
# output_file_name = output_file.pdf
# set output output_file_name

set xlabel "limit trace length"
# set xrange [30:130]

set ylabel "CFI number"
# set ylabel "second"
# set yrange [0:10000]

set key left

set grid xtics ytics

# plot "wget_results.dat" using 1:2 with lines
# plot "wget_results.dat" using 1:2 with linespoints title "branchable", \
#      "wget_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "wget_results.dat" using 1:5 with linespoints title "total"
# plot "wget_results.dat" using 1:6 with linespoints title "covering time"
# plot "curl_results.dat" using 1:2 with linespoints title "branchable", \
#      "curl_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "curl_results.dat" using 1:5 with linespoints title "total"
# plot "nginx_results.dat" using 1:2 with linespoints title "branchable", \
#      "nginx_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "nginx_results.dat" using 1:5 with linespoints title "total"
# plot "jwhois_results.dat" using 1:2 with linespoints title "branchable", \
#      "jwhois_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "jwhois_results.dat" using 1:5 with linespoints title "total"
# plot "netcat_results.dat" using 1:2 with linespoints title "branchable", \
#      "netcat_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "netcat_results.dat" using 1:5 with linespoints title "total"
# plot "links_results.dat" using 1:2 with linespoints title "branchable", \
#      "links_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "links_results.dat" using 1:5 with linespoints title "total"
# plot "links_results.dat" using 1:6 with linespoints title "covering time"

# plot "ncftpget_results.dat" using 1:2 with linespoints title "branchable", \
#      "ncftpget_results.dat" using 1:4 with linespoints title "unbranchable", \
#      "ncftpget_results.dat" using 1:5 with linespoints title "total"

# plot "ncftpget_results.dat" using 1:6 with linespoints title "covering time"
# plot "ncftpget_results.dat" using 1:6 with linespoints title "covering time"
plot "wgetftp_results.dat" using 1:6 with linespoints title "covering time"
