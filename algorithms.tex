% !TeX root = these.tex
% !TeX program = lualatex

% \section{Calculation}

This chapter can be considered as the calculation part, motivated from results of the previous chapter. We will give algorithms to calculate a normal form of a given labeled transition system. Let us first recall some obtained theoretical results.
\begin{enumerate}
  \item by~\Cref{def:observational_simulation}, to predict correctly a given LTS $\mathscr{A}$, a LTS must be an observational prediction of $\mathscr{A}$,

  \item by~\Cref{prop:full_morphism_observational_simulation,prop:state_equivalence_full_morphism}, such an observational prediction can be obtained as a quotient $\mathscr{A}/{\sim}$ of $\mathscr{A}$ by some nontrivial $\pi$-equivalence $\sim$ (notice that a $\pi$-equivalence is trivial if and only if it is the identity relation), and

  \item by~\Cref{def:reduction_sequence} and~\Cref{the:best_compact_possible}, a best compact possible prediction can be obtained as a normal form $\mathscr{A}_{n}$ of $\mathscr{A}$ (notice that $\mathscr{A}$ may have multiple normal forms but they are the same up to the number of states), resulted from an arbitrary reduction sequence.
  % limit $\mathscr{A}^{\lim \sqsubseteq_{op}}$ of a reduction sequence $\mathscr{A}^{\sqsubseteq_{op}}$ derived from $\mathscr{A}$
\end{enumerate}

\section{Compact labeled transition system construction}

Straightforwardly from~\Cref{def:reduction_sequence}, we have the algorithm in~\Cref{alg:lts_reduction} producing a normal form (i.e. a best compact possible observational prediction) of a labeled transition system.

% \renewcommand{\lstlistingname}{Algorithm}
\begin{lstlisting}[frame=lines, mathescape, caption={Label transition system reduction algorithm}, captionpos=b, label={alg:lts_reduction}]
LTSReduction(LTS $\mathscr{A}$) {
  $\mathscr{A}' \gets \mathscr{A}$;
  while (true) {
    $\sim \ \gets$ StateEquivCons($\mathscr{A}'$);
    if ($\sim$ is trivial) break; // no more nontrivial $\pi$-equivalence
    else $\mathscr{A}' \gets \mathscr{A}'/{\sim}$;
  }
  return $\mathscr{A}'$;
}
\end{lstlisting}

Let us start from the initial LTS $\mathscr{A}$, then look for a nontrivial $\pi$-equivalence $\sim$ in $\mathscr{A}$, we obtain next the quotient LTS $\mathscr{A}/{\sim}$ which has strictly fewer states than $\mathscr{A}$. This procedure is repeated with $\mathscr{A}$ is replaced by $\mathscr{A}/{\sim}$ until no more nontrivial $\pi$-equivalence is found.

\begin{remark}
Because the $\pi$-equivalences can be selected arbitrarily at each step, the returned labeled transition systems of Algorithm~\ref{alg:lts_reduction} are not unique (cf.~\Cref{exa:different_limits}), but their number of states are stable as given by~\Cref{the:best_compact_possible}.
\end{remark}

\section{\texorpdfstring{$\Pi$}{Pi}-equivalence verification and construction}

The crucial point of the LTS reduction algorithm is the procedure \texttt{State\-Equiv\-Cons} which returns a nontrivial $\pi$-equivalence in a LTS $\mathscr{A}$ whenever such a relation exists in $\mathscr{A}$. The co-inductive definition of the $\pi$-equivalence (cf.~\Cref{def:state_equivalence}) is not constructive: essentially it does not say about how to construct such a relation, instead it say about how to verify whether a given relation is a $\pi$-equivalence or not. 

We may note that the number of states in $\mathscr{A}$ is finite, then the number of binary relations is also finite, consequently a verification algorithm, by checking each subset of $Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right)$, gives indirectly a construction algorithm. This direct approach is practically infeasible because there are $2^{Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right)}$ subsets.

In~\Cref{alg:state_equivalence_verif}, we use a slightly different approach by first introduce a verification algorithm and then describe how to modify it to get a construction algorithm.

% \renewcommand{\lstlistingname}{Algorithm}
\begin{lstlisting}[frame=lines, mathescape, caption={$\pi$-equivalence verification algorithm}, captionpos=b, label={alg:state_equivalence_verif}]
StateEquivVerif(equivalence $R$) {
  $R_{unv} \gets \lbrace \left(q,q'\right),\left(q,q'\right) \in R \mid q \neq q', q \neq \bot, q' \neq \bot \rbrace$; // the set of unverified pairs
  $R_{ved} \gets R \setminus R_{unv}$;  // the set of verified pairs

  while ($R_{unv} \neq \emptyset$) {
    $\left(q,q'\right) \gets \text{ an element in } R_{unv}$;
    if (for each $q \xrightarrow{c} q_{1}$ there exists $q' \xrightarrow{c} q'_{1}$ and vice-versa) {
      $R_{ved} \gets R_{ved} \cup \lbrace \left(q,q'\right),\left(q',q\right) \rbrace$; $R_{unv} \gets R_{unv} \setminus \lbrace \left(q,q'\right),\left(q',q\right) \rbrace$;

      $R^{l} \gets \lbrace \left(q_{1},q'_{1}\right),\left(q'_{1},q_{1}\right) \mid \exists c\colon \ q \xrightarrow{c} q_{1}, q' \xrightarrow{c} q'_{1} \rbrace$;
      $R_{ved}^{l} \gets \lbrace \left(q,q'\right) \in R_{l} \mid \textnormal{either } p = q, \textnormal{ or } p = \bot \textnormal{ or } p' = \bot \rbrace$;
      $R_{ved} \gets R_{ved} \cup R_{ved}^{l}$; $R_{unv} \gets R_{unv} \cup (R^{l} \setminus R_{ved})$;

      if ($R_{ved} \cup R_{unv} \neq R$) return false;
    }
    else return false;
  }

  return true;
}
\end{lstlisting}

\begin{remark}
The input $R$ of the algorithm in~\Cref{alg:state_equivalence_verif} is an equivalence. To make the algorithm work with any relation, we can simply add a pre-processing procedure to verify whether $R$ is an equivalence or not before passing $R$ to the procedure $\texttt{State\-Equiv\-Verif}$. Also in the presentation of the algorithm, we write $q \neq \bot$ to mean that the state $q$ has some transitions.
\end{remark}

Algorithm~\ref{alg:state_equivalence_verif} verifies whether the input relation $R$ (as an equivalence) is a $\pi$-equivalence or not. The sets $R_{unv}$ and $R_{ved}$ contains respectively the unverified and verified pairs of states of $R$. Since the verification of reflexive pairs or pair containing some $\bot$ states is not necessary, these sets are initialized by
\begin{equation*}
  R_{unv} \gets \lbrace \left(q,q'\right),\left(q,q'\right) \in R \mid q \neq q', q \neq \bot, q' \neq \bot \rbrace \textnormal{ and } R_{ved} \gets R \setminus R_{unv}
\end{equation*}
Whenever there are still some pairs need to be verified (this is determined by checking whether $R_{unv}$ is empty or not), then takes a pair $\left(q,q'\right)$ from $R_{unv}$. The states $q$ and $q'$ are verified whether they satisfy the conditions of the $\pi$-equivalence or not, since $q \neq q'$ and $q \neq \bot, q' \neq \bot$, the verification means checking: for any transition $q \xrightarrow{c} q_{1}$, there exists also $q' \xrightarrow{c} q'_{1}$ for some states $q_{1}$ and $q'_{1}$ and vice-versa. If not then we can conclude immediately that $R$ is not a $\pi$-equivalence.
Otherwise, the sets $R_{unv}$ and $R_{ved}$ are updated to reflect that the pairs $\left(p,p'\right)$ and $\left(p',p\right)$ are verified:
\begin{equation*}
  R_{unv} \gets R_{unv} \setminus \lbrace \left(q,q'\right),\left(p',p\right) \rbrace \textnormal{ and } R_{ved} \gets R_{ved} \cup \lbrace \left(q,q'\right),\left(p',p\right) \rbrace
\end{equation*}
We construct the local set $R^{l}$ of corresponding targets from $q$ and $q'$:
\begin{equation*}
  R^{l} \gets \lbrace \left(q_{1},q'_{1}\right),\left(q'_{1},q_{1}\right) \mid \exists c\colon \ q \xrightarrow{c} q_{1}, q' \xrightarrow{c} q'_{1} \rbrace
\end{equation*}
Some pairs in $R^{l}$ do not need to be verified (such pairs are reflexive or contain some $\bot$ states), they are selected into a local set
\begin{equation*}
  R_{ved}^{l} \gets \lbrace \left(q,q'\right) \in R_{l} \mid \textnormal{either } p = q, \textnormal{ or } p = \bot \textnormal{ or } p' = \bot \rbrace
\end{equation*}
and the set $R_{ved}$ is updated: $R_{ved} \gets R_{ved} \cup R_{ved}^{l}$.
The local set of unverified pairs is determined by $R^{l} \setminus R_{ved}$, then $R_{unv}$ is updated: $R_{unv} \gets R_{unv} \cup \left(R^{l} \setminus R_{ved} \right)$.
At this point, both $R_{ved}$ and $R_{unv}$ are updated with new "local information", that is the set $R^{l}$ of corresponding targets from $q$ and $q'$, then we verify whether the invariant
\begin{equation*}
  R_{ved} \cup R_{unv} = R
\end{equation*}
is preserved or not. If not, there are some unverified pairs which should belong to $R$ but they are actually not, then $R$ cannot be a $\pi$-equivalence. If the verification can reach to the point where $R_{unv}$ is empty, namely all pairs are verified and passed, then $R$ is a $\pi$-equivalence.

\begin{lemma}\label{lem:state_equivalence_verif}
Given an equivalence $R$, the procedure \texttt{State\-Equiv\-Verif} of the input $R$ halts always, and it returns \texttt{true} if and only if $R$ is a $\pi$-equivalence.
\end{lemma}
\begin{proof}
Let $R_{unv}^{i}$ and $R_{ved}^{i}$ denote values of the set $R_{unv}$ and $R_{ved}$ at the beginning of $i$-th while-step. We prove the invariants $R = R_{unv}^{i} \cup R_{ved}^{i}$ and $\lvert R_{ved}^{i+1} \rvert = \lvert R_{ved}^{i} \rvert - 2$ for all $i$.
Indeed, this is already true for $i = 1$. This is also true at the beginning of the $\left(i+1\right)$-th while-step, because the commands in the if-condition of the $i$-th while-step have been fully executed. The invariants are proved

Since $\lvert R_{ved}^{i+1} \rvert = \lvert R_{ved}^{i} \rvert - 2$, $\texttt{State\-Equiv\-Verif}\left(R\right)$ halts after no more than $\sfrac{\lvert R \rvert}{2}$ while-step. We observe that the commands inside each while-step verify the conditions of the $\pi$-equivalence (cf.~\Cref{def:state_equivalence}).
Moreover, $\texttt{State\-Equiv\-Verif}\left(R\right)$ returns true if and only if it halts at $R \setminus R_{ved} = R_{unv} = \emptyset$, that means all pairs in $R$ are verified and passed.
\end{proof}

\begin{corollary}
The worst-case (time) complexity of Algorithm~\ref{alg:state_equivalence_verif} is $\mathcal{O}\left(\lvert R \rvert\right)$.
\end{corollary}

In Algorithm~\ref{alg:state_equivalence_verif}, if the verification of the invariant $R = R_{ved} \cup R_{unv}$ (as the last command of each while-step) is always executed and passed then $R$ is eventually a $\pi$-equivalence. That means, if, instead of keep the relation $R$ as a fixed input, we update it by
\begin{equation*}
  R \gets R_{ved} \cup R_{unv}
\end{equation*}
then Algorithm~\ref{alg:state_equivalence_verif} becomes automatically a construction algorithm, but to make it return an equivalence, the local set $R_{l}$ need to be set appropriately.

% \renewcommand{\lstlistingname}{Algorithm}
\begin{lstlisting}[frame=lines, mathescape, caption={$\pi$-equivalence construction algorithm}, captionpos=b, label={alg:state_equivalence_construction}]
StateEquivCons(LTS $\mathscr{A}$) {
  $R \gets \lbrace \left(q,q\right) \mid q \in Q\left(\mathscr{A}\right) \rbrace$; // the trivial $\pi$-equivalence

  foreach ($\left(q,q'\right) \in Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right), q \neq q'$) {
    $R_{unv} \gets \lbrace \left(q,q'\right),\left(q',q\right)\rbrace$; // the set of unverified pairs
    $R_{ved} \gets \lbrace \left(q,q\right) \mid q \in Q\left(\mathscr{A}\right) \rbrace$; // the set of verified pairs
    $R \gets R_{ved}$;

    while ($R_{unv} \neq \emptyset$) {
      $\left(q,q'\right) \gets$ an element of $R_{unv}$;
      if (for each $q \xrightarrow{c} q_{1}$ there exists $q' \xrightarrow{c} q'$ and vice-versa) {
        $R_{unv} \gets R_{unv} \setminus \lbrace \left(q,q'\right),\left(q',q\right) \rbrace$; $R_{ved} \gets R_{ved} \cup \lbrace \left(q,q'\right),\left(q,q'\right) \rbrace$;

        $R_{l} \gets \lbrace \left(q_{1},q'_{1} \right),\left(q'_{1},q_{1} \right) \mid \exists c: \ q \xrightarrow{c} q_{1},q' \xrightarrow{c} q'_{1}\rbrace$;
        $R_{l} \gets R_{l} \setminus R_{ved}$; $R_{unv} \gets R_{unv} \cup R_{l}$;

        $R \gets R_{ved} \cup R_{unv}$; $R \gets \text{transitive-symmetric closure of } R$;
        $R_{unv} \gets R \setminus R_{ved}$;
      }
      else break;
    } // end of while

    if ($R_{unv} = \emptyset$) return $R$;
  } // end of foreach

  return $R$;
}
\end{lstlisting}

The algorithm in~\Cref{alg:state_equivalence_construction} returns a nontrivial $\pi$-equivalence whenever such a relation exists in the input LTS $\mathscr{A}$, otherwise it returns the trivial $\pi$-equivalence. In fact, it does what one in~\Cref{alg:state_equivalence_verif} does but in the opposite direction: the verification
\begin{equation*}
  \textnormal{if } \left(R_{ved} \cup R_{unv} \neq R \right) \textnormal{ return false}
\end{equation*}
is replaced by the construction
\begin{equation*}
  R \gets R_{ved} \cup R_{unv}; \ R \gets \textnormal{transitive-symmetric closure of } R
\end{equation*}

\begin{proposition}
The procedure \texttt{State\-Equiv\-Cons} halts always. It returns a nontrivial $\pi$-equivalence whenever such a relation exists in $\mathscr{A}$, otherwise it returns the trivial $\pi$-equivalence.
\end{proposition}
\begin{proof}
We can recognize that the while-loop in \texttt{State\-Equiv\-Cons} is a traditional technique to calculate the relation $R$ as a fixed-point.
To make it clear, for each pair $\left(q,q'\right) \in Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right)$ and $q \neq q'$,
let $R^{i}, R_{unv}^{i}$ and $R_{ved}^{i}$ denote respectively value of the sets $R$, $R_{unv}$ and $R_{ved}$ just after the last command of the "if ..." block (i.e. after the update $R_{unv} \gets R \setminus R_{ved}$ of the $i$-th while-step. We prove that:
\begin{enumerate*}
  \item $R^{i}$ is an equivalence, $R^{i} \subseteq R^{i+1}$
  \item $R^{i} = R_{unv}^{i} \cup R_{ved}^{i}$, and
  \item $R_{ved}^{i} \subsetneq R_{ved}^{i+1}$
\end{enumerate*}
for all $i$.

Indeed, we have $\lbrace \left(q,q\right) \mid q \in Q\left(\mathscr{A}\right) \rbrace \subseteq R^{i}$ for all $i$, then $R^{i}$ is reflexive. Moreover $R^{i}$ is also transitive and symmetric at the end of each while-step, so $R^{i}$ is an equivalence. We have also $R^{i+1}$ is the transitive-symmetric closure of $R^{i} \cup R_{l}$, then $R^{i} \subseteq R^{i+1}$. From the last command $R_{unv} \gets R \setminus R_{ved}$ in the "if ..." block, then $R^{i} = R_{unv}^{i} \cup R_{ved}^{i}$.
Also in each while-step, we have $R_{ved}^{i+1} = R_{ved}^{i} \cup \lbrace \left(q,q'\right),\left(q,q'\right) \rbrace$, then $R_{ved}^{i} \subsetneq R_{ved}^{i+1}$.

Since $R_{ved}^{i} \subsetneq R_{ved}^{i+1} \subseteq Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right)$, the while-loop must halt at some value $n$ of $i$, consequently
\texttt{State\-Equiv\-Cons} halts always and returns
\begin{enumerate*}
  \item either $R = R^{n}$ if there is some pair $\left(q,q'\right) \in Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right)$ and $q \neq q'$ satisfying $q \sim q'$ for any $\pi$-equivalence $\sim$ (in this case we have also $R \subseteq \sim$),

  \item or the trivial $\pi$-equivalence.
\end{enumerate*}
\end{proof}

\begin{proposition}
The average-case (time) complexity of Algorithm~\ref{alg:state_equivalence_construction} is ${\lvert Q\left(\mathscr{A}\right) \rvert}^{2}$.
\end{proposition}
\begin{proof}
For any pair $\left(q,q'\right) \in Q\left(\mathscr{A}\right) \times Q\left(\mathscr{A}\right)$ and $q \neq q'$, let $\sim_{pq}$ denote the largest $\pi$-equivalence containing $\left(q,q'\right)$ (we have $\sim_{pq} = \emptyset$ if $q \not\sim q'$). Selecting such a pair, Algorithm~\ref{alg:state_equivalence_construction} returns either the minimal $\pi$-equivalence $\sim_{pq}^{min}$ containing $\left(p,q\right)$ or the trivial $\pi$-equivalence after no more than $\lvert \sim_{pq}^{min} \rvert$ computation steps.
\end{proof}